INTRODUCTION
------------

Users sometimes forget their usernames. Username Reminder provides a form for
requesting a username reminder, which is linked from the user login block and
the user page. The user who has forgotten their username can enter their email
address into the form, and if it matches an active account, the account
username is emailed to the email address.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/username_reminder

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/username_reminder


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. For further
information, visit:
  https://drupal.org/documentation/install/modules-themes/modules-8


CONFIGURATION
-------------

The subject and body of the reminder email are customizable via Administration
» Configuration » People » Username Reminder. Token replacements are
supported.


CREDITS
-------

Thanks to Steve Krzysiak for the Forgot Username module [0], which was a useful
reference for designing and implementing this module.

[0] https://github.com/stkrzysiak/forgot-username-drupal-module
