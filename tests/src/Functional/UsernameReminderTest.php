<?php

namespace Drupal\Tests\username_reminder\Functional;

use Drupal\Core\Test\AssertMailTrait;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests requesting a username reminder.
 *
 * @group UsernameReminder
 */
class UsernameReminderTest extends BrowserTestBase {

  use AssertMailTrait;

  /**
   * {@inheritdoc}
   */
  public static $modules = ['username_reminder'];

  /**
   * Tests requesting username for unknown email address.
   */
  public function testUsernameReminderUnknownEmail() {
    $this->drupalGet('user/username');
    $email = 'foo@example.com';
    $edit = ['email' => $email];
    $this->submitForm($edit, 'Submit');
    $this->assertSession()->pageTextContains('Sorry, ' . $email . ' is not a recognized email address.', 'Message indicates unknown email address.');
  }

  /**
   * Tests requesting username for active account.
   */
  public function testUsernameReminderActiveAccount() {
    $this->drupalGet('user/username');
    $account = $this->createUser();
    $edit = ['email' => $account->getEmail()];
    $this->submitForm($edit, 'Submit');
    $this->assertSession()->pageTextContains('Your username has been sent to your email address.', 'Message indicates username reminder sent.');
    $token_service = \Drupal::token();
    $expected_email = [
      'subject' => $token_service->replace("[site:name] username reminder"),
      'body' => $token_service->replace("Your username: [user:name]\n\n--  [site:name] team", ['user' => $account]) . "\n",
    ];
    $this->assertMail('subject', $expected_email['subject'], 'Sent email subject matches expectation.');
    $this->assertMail('body', $expected_email['body'], 'Sent email body matches expectation.');
  }

  /**
   * Tests requesting username for inactive account.
   */
  public function testUsernameReminderInactiveAccount() {
    $this->drupalGet('user/username');
    $account = $this->createUser();
    $account->block();
    $account->save();
    $edit = ['email' => $account->getEmail()];
    $this->submitForm($edit, 'Submit');
    $this->assertSession()->pageTextContains('Sorry, ' . $account->getEmail() . ' is not a recognized email address.', 'Message indicates unknown email address.');
  }

  /**
   * Tests customizing username reminder email.
   */
  public function testCustomUsernameReminder() {
    // Customize email.
    $admin = $this->createUser(['administer username reminder']);
    $this->drupalLogin($admin);
    $this->drupalGet('admin/config/people/username_reminder');
    $edit = [
      'username_reminder_subject' => 'foo',
      'username_reminder_body' => 'bar: [user:name]',
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->drupalLogout();

    // Request username reminder.
    $this->drupalGet('user/username');
    $account = $this->createUser();
    $edit = ['email' => $account->getEmail()];
    $this->submitForm($edit, 'Submit');
    $this->assertSession()->pageTextContains('Your username has been sent to your email address.', 'Message indicates username reminder sent.');
    $token_service = \Drupal::token();
    $expected_email = [
      'subject' => 'foo',
      'body' => $token_service->replace("bar: [user:name]", ['user' => $account]) . "\n",
    ];
    $this->assertMail('subject', $expected_email['subject'], 'Sent email subject matches expectation.');
    $this->assertMail('body', $expected_email['body'], 'Sent email body matches expectation.');
  }

}
